package com.example.jharshman.jdharshmanlab5;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    private TextView mTextView;

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        mTextView = (TextView)view.findViewById(R.id.textview_fragment_detail);

        if(getArguments() != null) {
            String arg = getArguments().getString("key");
            setTextView(arg);
        }

        return view;
    }

    public void setTextView(String value) {
        //mTextView = (TextView)getView().findViewById(R.id.textview_fragment_detail);
        mTextView.setText(value);
    }

}
