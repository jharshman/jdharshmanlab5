package com.example.jharshman.jdharshmanlab5;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements MainFragment.UpdateListener {

    private final String DETAIL_FRAG = "DETAIL_FRAGMENT";

    private DetailFragment mDetailFragment;
    private MainFragment mMainFragment;
    private FragmentManager fragmentManager;

    private String mValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getFragmentManager();

        if(!checkLandscape()) {
            fragmentManager.beginTransaction()
                    .add(R.id.PortHolder, new MainFragment())
                    .commit();
        } else {
            mDetailFragment = (DetailFragment)fragmentManager.findFragmentById(R.id.detailHolder);
            if(!savedInstanceState.isEmpty()) {
                mDetailFragment.setTextView(savedInstanceState.getString("saved"));
            }
            fragmentManager.popBackStack(0, fragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            aboutMe();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUpdateInteraction(String value) {

        mValue = value;

        if(checkLandscape()) {
            mDetailFragment = (DetailFragment)fragmentManager.findFragmentById(R.id.detailHolder);
            mDetailFragment.setTextView(mValue);
            Log.d("LandScape Mode"," True");
        } else {
            //not in dual pane mode
            if(getFragmentManager().findFragmentByTag(DETAIL_FRAG) != null) {
                Log.d("Detail Fragment - ", " Existing Fragment Found");
            } else {
                //instantiate the fragment

                Log.d("Detail Fragment - ", " Creating New Fragment");
                mDetailFragment = new DetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("key", mValue);
                mDetailFragment.setArguments(bundle);

                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.PortHolder, mDetailFragment, DETAIL_FRAG)
                        .commit();

            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("saved", mValue);
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }

    public void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    /**
     * simple check on device orientation
     * @return true if device is in landscape mode
     */
    private boolean checkLandscape() {
        return getResources().getBoolean(R.bool.dual_pane);
    }
}
